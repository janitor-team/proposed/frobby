Document: libfrobby-dev
Title: Frobby Documentation
Author: Bjarke Hammersholt Roune
Abstract: Frobby is a program and library for performing computations related
 to monomial ideals, such as Hilbert-Poincare series and irreducible
 decomposition.Frobby is loosely structured into a number of layers. These are
 the bottom layer which performs the actual computations, the middle layer that
 gives the functionality in the bottom layer a more convenient interface, and
 the top layer which consists of Frobby’s interface to the world as a library
 or console program.
 .
 The interface to Frobby as a library is in the file frobby.h. The console
 layer has more code to it, where the central classes are those derived from
 the classes Action and IOHandler.
 .
 The middle layer is composed of a number of classes following the Facade
 design pattern, and they all derive from the class Facade.
 .
 The bottom layer consists of various classes and functions that perform
 calculations on monomial ideals. The main functionality is accessed by using
 the function runSliceAlgorithm, which runs the Slice Algorithm on a monomial
 ideal. The Slice Algorithm can be specialized to compute a number of things,
 and these specializations are implemented as derived classes of SliceStrategy,
 which can be passed as a parameter to runSliceAlgorithm.
Section: Science/Mathematics

Format: HTML
Index: /usr/share/doc/libfrobby-dev/html/index.html
Files: /usr/share/doc/libfrobby-dev/html/*
